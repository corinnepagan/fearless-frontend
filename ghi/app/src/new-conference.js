import React, {useEffect, useState} from 'react';

function ConferenceForm (props) {
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [start, setStart] = useState('');
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }
    const [end, setEnd] = useState('');
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }
    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const [presentation, setPresentation] = useState('');
    const handlePresentationChange = (event) => {
        const value = event.target.value;
        setPresentation(value);
    }
    const [attendee, setAttendee] = useState('');
    const handleAttendeeChange = (event) => {
        const value = event.target.value;
        setAttendee(value);
    }
    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = presentation;
        data.max_attendees = attendee;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setPresentation('');
            setAttendee('');
            setLocation('');
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" value={name} name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className=" mb-3">
                <input onChange={handleStartChange} type="date" id="starts" value={start} name="starts"/>
                <label htmlFor="start_date">Start date</label>
              </div>
              <div className="mb-3">
                <input onChange={handleEndChange} type="date" id="ends" value={end} name="ends"/>
                <label htmlFor="end_date">End date</label>
              </div>
              <div className="mb-3">
                <textarea onChange={handleDescriptionChange} placeholder="Description" required type="text" id="description" value={description} name="description" className="form-control"></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className=" mb-3">
                <input onChange={handlePresentationChange} type="number" id="max_presentations" value={presentation} name="max_presentations" min="1" max="200"/>
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>
              <div className="mb-3">
                <input onChange={handleAttendeeChange} type="number" id="max_attendees" value={attendee} name="max_attendees" min="1" max="200"/>
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" value={location} name="location" className="form-select">
                  <option value=''>Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option value={location.id} key={location.id}>
                        {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    );
    }

    export default ConferenceForm;
