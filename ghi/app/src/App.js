import Nav from './Nav';
// import AttendeesList from './AttendeesList';
// import LocationForm from './LocationForm';
// import ConferenceForm from './new-conference';
import AttendConference from './attend-conference';
function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />

    <div className="container">
      <AttendConference />
      {/* <ConferenceForm/> */}
      {/* <LocationForm /> */}
   {/* <AttendeesList attendees={props.attendees} /> */}
    </div>
    </>
  );
}

export default App;
